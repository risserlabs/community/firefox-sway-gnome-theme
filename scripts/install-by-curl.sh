#!/usr/bin/env bash

(
cd $(mktemp -d) || exit 1
mkdir firefox-gnome-theme
cd firefox-gnome-theme
curl -LJo theme.tar.gz https://gitlab.com/risserlabs/community/firefox-sway-gnome-theme/-/archive/master/firefox-sway-gnome-theme-master.tar.gz
tar -xzf theme.tar.gz --strip-components=1
chmod +x scripts/auto-install.sh
yes | ./scripts/auto-install.sh
)
